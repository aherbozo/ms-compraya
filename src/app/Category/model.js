const mongoose = require("mongoose");
const categorySchema = mongoose.Schema({
  idCategory: {
    type: Number,
    required: [true, "Please include the product price"],
  },
  name: {
    type: String,
    required: [true, "Please include the product name"],
  }
});
const Category = mongoose.model("Category", categorySchema);
module.exports = Category;