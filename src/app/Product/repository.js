const Product = require("./model");
exports.products = async () => {
    const products = await Product.aggregate([
    {
        "$lookup":{
            from: 'categorys',
            localField: 'idCategory',
            foreignField: 'idCategory',
            as: 'category'
        }
    },
    {
        "$lookup":{
            from: 'brands',
            localField: 'brandId',
            foreignField: 'idBrand',
            as: 'brand'
        }
    },
    {
        "$lookup":{
            from: 'subCategorys',
            localField: 'subCategoryId',
            foreignField: 'idSubCategory',
            as: 'subCategory'
        }
    },
    {
        "$lookup":{
            from: 'prices',
            localField: 'idPrice',
            foreignField: 'idPrice',
            as: 'price'
        }
    },
    {
        "$project":{
            _id:0,
            0:0
        }
    },{
        "$unwind":{
            path: "$category",
            includeArrayIndex: '0',
            preserveNullAndEmptyArrays: true
          }
    },
    {
        "$unwind":{
            path: "$brand",
            includeArrayIndex: '0',
            preserveNullAndEmptyArrays: true
          }
    },
    {
        "$unwind":{
            path: "$subCategory",
            includeArrayIndex: '0',
            preserveNullAndEmptyArrays: true
          }
    },
    {
        "$unwind":{
            path: "$price",
            includeArrayIndex: '0',
            preserveNullAndEmptyArrays: true
          }
    }
    ]);

    return products;
};
exports.productById = async id => {
    const product = await Product.findById(id);
    return product;
}
exports.createProduct = async payload => {
    const newProduct = await Product.create(payload);
    return newProduct
}
exports.removeProduct = async id => {
    const product = await Product.findByIdAndRemove(id);
    return product
}