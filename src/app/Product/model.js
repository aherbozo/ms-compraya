const mongoose = require("mongoose");

const productSchema = mongoose.Schema({
  productId: {
    type: Number,
    required: [true, "Please include the product price"],
  },
  name: {
    type: String,
    required: [true, "Please include the product name"],
  },
  description: {
    type: String,
    required: [true, "Please include the product price"],
  },
  detail: {
    type: String,
    required: [true, "Please include the product price"],
  },
  idCategory: {
    type: Number,
    required: [true, "Please include the product price"],
  },
  subCategoryId: {
    type: Number,
    required: [true, "Please include the product price"],
  },
  initialStock: {
    type: Number,
    required: [true, "Please include the product price"],
  },
  initialReal: {
    type: Number,
    required: [true, "Please include the product price"],
  },
  status: {
    type: Number,
    required: [true, "Please include the product price"],
  },
  image: {
      type: String
    },
  brandId: {
    type: Number,
    required: [true, "Please include the product price"],
  },
  idPrice: {
    type: Number,
    required: [true, "Please include the product price"],
  }
});
const Product = mongoose.model("Product", productSchema);
module.exports = Product;